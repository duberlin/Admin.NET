﻿// 大名科技（天津）有限公司 版权所有
//
// 此源代码遵循位于源代码树根目录中的 LICENSE 文件的许可证
//
// 不得利用本项目从事危害国家安全、扰乱社会秩序、侵犯他人合法权益等法律法规禁止的活动
//
// 任何基于本项目二次开发而产生的一切法律纠纷和责任，均与作者无关

namespace Admin.NET.Core;

/// <summary>
/// Claim相关常量
/// </summary>
public class ClaimConst
{
    /// <summary>
    /// 用户Id
    /// </summary>
    public const string UserId = "UserId";

    /// <summary>
    /// 账号
    /// </summary>
    public const string Account = "Account";

    /// <summary>
    /// 真实姓名
    /// </summary>
    public const string RealName = "RealName";

    /// <summary>
    /// 昵称
    /// </summary>
    public const string NickName = "NickName";

    /// <summary>
    /// 账号类型
    /// </summary>
    public const string AccountType = "AccountType";

    /// <summary>
    /// 租户Id
    /// </summary>
    public const string TenantId = "TenantId";

    /// <summary>
    /// 组织机构Id
    /// </summary>
    public const string OrgId = "OrgId";

    /// <summary>
    /// 组织机构名称
    /// </summary>
    public const string OrgName = "OrgName";

    /// <summary>
    /// 组织机构类型
    /// </summary>
    public const string OrgType = "OrgType";

    /// <summary>
    /// 微信OpenId
    /// </summary>
    public const string OpenId = "OpenId";

    /// <summary>
    /// 登录模式PC、APP
    /// </summary>
    public const string LoginMode = "LoginMode";
}